#!/usr/bin/python3

import Jetson.GPIO as GPIO
import time

# Set the GPIO mode to BOARD numbering
GPIO.setmode(GPIO.BOARD)

print("TESTING IR LED and LED")
print("Press Ctrl+C to continue with the next test")
# Set the GPIO mode to BOARD numbering
GPIO.setmode(GPIO.BOARD)

# Set the GPIO pin number you want to control
gpio_pin_IR = 33

# Set the GPIO pin as an output
GPIO.setup(gpio_pin_IR, GPIO.OUT)

try:
    while True:
        # Turn the GPIO pin on
        GPIO.output(gpio_pin_IR, GPIO.HIGH)
        print("GPIO pin turned ON")
        time.sleep(0.5)

        # Turn the GPIO pin off
        GPIO.output(gpio_pin_IR, GPIO.LOW)
        print("GPIO pin turned OFF")
        time.sleep(0.5)

except KeyboardInterrupt:
    # Clean up GPIO configuration on Ctrl+C
    GPIO.cleanup()

print("TESTING PHOTO DIODE, PLEASE BLOCK/UNBLOCK THE SENSOR WITH YOUR FINGERS")
print("Press Ctrl+C to finish testing")
# Pin Setup:
GPIO.setmode(GPIO.BOARD)  # Use the BOARD pin-numbering scheme
input_pin = 32  # Assuming GPIO Pin 7 is used

# Set pin as an input pin with a pull-up resistor
GPIO.setup(input_pin, GPIO.IN)

try:
    while True:
        # Read the input state
        input_state = GPIO.input(input_pin)

        # Get the current time
        current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

        # Print the pin status and current time
        print(f"{current_time} - Pin {input_pin} is {'HIGH' if input_state else 'LOW'}")

        # Delay before reading again
        time.sleep(0.1)

except KeyboardInterrupt:
    # Clean up and release GPIO resources when the script is terminated
    GPIO.cleanup()
